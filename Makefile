APP = ./gaku
SHELL = bash
include .bingo/Variables.mk
export

remove-merged-branches:
	git fetch --prune
	git branch --merged | egrep -v "(^\*|main)" | xargs git branch -d

lint:
	go run github.com/golangci/golangci-lint/cmd/golangci-lint@v1.42.1 run ./... -v --timeout 5m

lint-shell:
	shellcheck ./*.sh

tidy:
	./build/tidy.sh

lsif:
	go run github.com/sourcegraph/lsif-go/cmd/lsif-go@latest

junit: $(GO_JUNIT_REPORT)
	go test -covermode=count -coverprofile=coverage.txt ./... -v 2>&1 | $(GO_JUNIT_REPORT) -set-exit-code > report.xml
	go tool cover -html=coverage.txt -o index.html
	go tool cover -func=coverage.txt

.PHONY: build

# === Helper around "bingo get" ===
# See https://stackoverflow.com/questions/2214575/passing-arguments-to-make-run
# Use it like "make get github.com/mypackage/cmd"

# If the first argument is "get"...
ifeq (get,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "get"
  GET_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(GET_ARGS):;@:)
endif

.PHONY: get
get: $(BINGO)
	$(BINGO) get $(GET_ARGS)