package main

import (
	"gitlab.com/hackandsla.sh/gaku/cmd"
)

//nolint:gochecknoglobals // These variables may be dynamically inserted at build-time
var (
	version = ""
	commit  = ""
	date    = ""
	builtBy = ""
	appName = "gaku"
)

func main() {
	buildInfo := &cmd.BuildInfo{
		App:     appName,
		Version: version,
		Commit:  commit,
		Date:    date,
		BuiltBy: builtBy,
	}

	cmd.Execute(buildInfo)
}
