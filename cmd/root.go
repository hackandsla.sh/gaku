package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/hackandsla.sh/gaku/internal"
)

func Execute(info *BuildInfo) {
	var rootCmd = initRootCmd(info)

	if err := rootCmd.Execute(); err != nil {
		exit(err)
	}
}

func initRootCmd(info *BuildInfo) *cobra.Command {
	cobra.OnInitialize(getInitConfig())

	cmd := &cobra.Command{
		Use:     info.App,
		Short:   "GitLab Authorized Key Updater",
		Long:    "Manage authorized keys through GitLab",
		Version: info.Version,
		Run: func(cmd *cobra.Command, args []string) {
			var config internal.Config
			if err := viper.Unmarshal(&config); err != nil {
				exit(err)
			}

			if err := internal.Run(config); err != nil {
				exit(err)
			}
		},
	}

	cmd.AddCommand(cmdVersion(info))

	bindFlags(cmd)

	return cmd
}

func bindFlags(cmd *cobra.Command) {
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.SetEnvPrefix("GAKU")

	cmd.Flags().StringArrayP("user", "u", nil, "The GitLab user to use")
	_ = viper.BindPFlag("users", cmd.Flags().Lookup("user"))

	cmd.Flags().BoolP("auto-delete", "a", false, "Automatically delete any keys that need deletion without prompting first")
	_ = viper.BindPFlag("autodelete", cmd.Flags().Lookup("auto-delete"))

	cmd.Flags().Bool("no-ignore", false, "Don't ignore any keys that have been previously skipped")
	_ = viper.BindPFlag("noignore", cmd.Flags().Lookup("no-ignore"))
}

func getInitConfig() func() {
	return func() {
		configDir, err := os.UserConfigDir()
		if err != nil {
			exit(fmt.Errorf("couldn't find user config dir: %w", err))
		}

		configPath := filepath.Join(configDir, "gaku")

		viper.SetConfigName("gaku")
		viper.SetConfigType("yaml")
		viper.AddConfigPath(configPath)

		if err := viper.ReadInConfig(); err != nil {
			if _, ok := err.(viper.ConfigFileNotFoundError); !ok {
				exit(fmt.Errorf("couldn't read config file: %w", err))
			}
		}
	}
}

func exit(err error) {
	color.Red("error: %v", err.Error())
	os.Exit(1)
}
