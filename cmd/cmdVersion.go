package cmd

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/spf13/cobra"
)

type BuildInfo struct {
	App     string `json:"app"`
	Version string `json:"version"`
	Commit  string `json:"commit"`
	Date    string `json:"date"`
	BuiltBy string `json:"built_by"`
}

func cmdVersion(info *BuildInfo) *cobra.Command {
	return &cobra.Command{
		Use:   "version",
		Short: "Display the version information of this binary",
		Long:  "Displays the version information for this binary as a JSON object",
		Run: func(cmd *cobra.Command, args []string) {
			j, err := json.MarshalIndent(&info, "", "  ")
			if err != nil {
				log.Fatalf("error while displaying version: %v", err)
			}

			fmt.Println(string(j))
		},
	}
}
