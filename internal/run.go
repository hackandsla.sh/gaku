package internal

import (
	"bufio"
	"bytes"
	"context"
	"crypto/md5" //nolint:gosec // This is only used for MD5 fingerprinting, which is a common format for SSH keys
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/fatih/color"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/google/renameio"
	"golang.org/x/crypto/ssh"
	"golang.org/x/sync/errgroup"
	yaml "gopkg.in/yaml.v3"

	"gitlab.com/hackandsla.sh/gaku/internal/gitlab"
)

type Config struct {
	Users      []string      `json:"users,omitempty" yaml:"users,omitempty"`
	Ignore     []Fingerprint `json:"ignore,omitempty" yaml:"ignore,omitempty"`
	AutoDelete bool          `json:"-" yaml:"-"`
	NoIgnore   bool          `json:"-" yaml:"-"`
}

func (c Config) Validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.Users, validation.Required),
	)
}

func Run(cfg Config) error {
	if err := cfg.Validate(); err != nil {
		return err
	}

	if cfg.NoIgnore {
		cfg.Ignore = nil
	}

	configDir, err := os.UserConfigDir()
	if err != nil {
		return fmt.Errorf("error while discovering configuration directory: %w", err)
	}

	gakuConfigDir := filepath.Join(configDir, "gaku")

	cfgPerms := os.ModeDir | 0700
	if err = os.Mkdir(gakuConfigDir, cfgPerms); err != nil && !errors.Is(err, fs.ErrExist) {
		return fmt.Errorf("error while creating config directory at '%s': %w", gakuConfigDir, err)
	}

	homeDir, err := os.UserHomeDir()
	if err != nil {
		return fmt.Errorf("error while discovering home directory: %w", err)
	}

	historyFile := newHistoryFile(filepath.Join(gakuConfigDir, "gaku_history.yaml"))
	configFile := newConfigFile(filepath.Join(gakuConfigDir, "gaku.yaml"))
	authorizedKeysFile := newAuthorizedKeysFile(filepath.Join(homeDir, ".ssh", "authorized_keys"))

	currentKeys, err := authorizedKeysFile.GetAuthorizedKeys()
	if err != nil {
		return fmt.Errorf("error while reading authorized_keys: %w", err)
	}

	history, err := historyFile.GetHistory()
	if err != nil {
		return fmt.Errorf("error while reading history file: %w", err)
	}

	newKeys, err := getGitlabKeys(cfg.Users)
	if err != nil {
		return err
	}

	addedKeys := newKeys.
		excluding(currentKeys).
		excludeFingerprints(cfg.Ignore)

	missingKeys := currentKeys.
		intersectingFingerprints(history.AddedFingerprints()). // Only look at keys previously added by this tool
		excluding(newKeys).                                    // Exclude keys currently in GitLab
		excludeFingerprints(cfg.Ignore)                        // Exclude keys that have been ignored

	var selectedToAdd, skippedAdded, selectedToDelete, skippedDeleted sshKeys

	if cfg.AutoDelete {
		selectedToDelete = missingKeys
	} else {
		selectedToAdd, skippedAdded = selectKeysToAdd(addedKeys)
		selectedToDelete, skippedDeleted = selectKeysToDelete(missingKeys)
	}

	newAuthorizedKeys := append(currentKeys, selectedToAdd...).excluding(selectedToDelete)

	if err := authorizedKeysFile.SetAuthorizedKeys(newAuthorizedKeys); err != nil {
		return fmt.Errorf("error while updating authorized_keys: %w", err)
	}

	history = append(history, newHistoryEntries(selectedToAdd.fingerprints(), HistoryEntryActionAdded)...)
	history = append(history, newHistoryEntries(selectedToDelete.fingerprints(), HistoryEntryActionRemoved)...)

	if err := historyFile.SetHistory(history); err != nil {
		return fmt.Errorf("error while updating history file: %w", err)
	}

	skippedFingerprints := append(skippedAdded.fingerprints(), skippedDeleted.fingerprints()...)

	cfg.Ignore = append(cfg.Ignore, skippedFingerprints...)

	if err := configFile.SetConfig(cfg); err != nil {
		return fmt.Errorf("error while updating configuration file: %w", err)
	}

	return nil
}

type Fingerprint string

type sshKey struct {
	Type    string
	Key     []byte
	Comment string
}

func (k sshKey) equals(other sshKey) bool {
	return k.Type == other.Type &&
		bytes.Equal(k.Key, other.Key)
}

func (k sshKey) fingerprint() Fingerprint {
	return fingerprint(k.Key)
}

func (k sshKey) String() string {
	return fmt.Sprintf("'%s' (%s)", k.Comment, k.fingerprint())
}

func (k sshKey) marshal() []byte {
	s := fmt.Sprintf("%s %s %s",
		k.Type,
		base64.StdEncoding.EncodeToString(k.Key),
		k.Comment,
	)

	return []byte(s)
}

type sshKeys []sshKey

func (keys sshKeys) excluding(other sshKeys) sshKeys {
	var diff sshKeys

	for _, key := range keys {
		if !other.contains(key) {
			diff = append(diff, key)
		}
	}

	return diff
}

func (keys sshKeys) excludeFingerprints(fingerprints []Fingerprint) sshKeys {
	var filtered sshKeys

	for _, key := range keys {
		if !fingerprintInSlice(key.fingerprint(), fingerprints) {
			filtered = append(filtered, key)
		}
	}

	return filtered
}

func (keys sshKeys) intersectingFingerprints(fingerprints []Fingerprint) sshKeys {
	var filtered sshKeys

	for _, key := range keys {
		if fingerprintInSlice(key.fingerprint(), fingerprints) {
			filtered = append(filtered, key)
		}
	}

	return filtered
}

func (keys sshKeys) contains(k sshKey) bool {
	for _, key := range keys {
		if key.equals(k) {
			return true
		}
	}

	return false
}

func (keys sshKeys) marshal() []byte {
	var buf []byte
	for _, key := range keys {
		buf = append(buf, key.marshal()...)
		buf = append(buf, '\n')
	}

	return buf
}

func (keys sshKeys) fingerprints() []Fingerprint {
	var f []Fingerprint

	for _, key := range keys {
		f = append(f, key.fingerprint())
	}

	return f
}

func fingerprint(key []byte) Fingerprint {
	//nolint:gosec // This is just used for MD5 fingerprinting, a common format for SSH key
	hasher := md5.New()
	hasher.Write(key)

	encoded := hex.EncodeToString(hasher.Sum(nil))
	charsPerBytes := 2
	split := splitEveryN(encoded, charsPerBytes)

	return Fingerprint(strings.Join(split, ":"))
}

func splitEveryN(s string, n int) []string {
	var ret []string

	for {
		if len(s) < n {
			if len(s) > 0 {
				ret = append(ret, s)
			}

			return ret
		}

		ret = append(ret, s[0:n])
		s = s[n:]
	}
}

func parseKey(key []byte) (sshKey, error) {
	out, comments, _, _, err := ssh.ParseAuthorizedKey(key)
	if err != nil {
		return sshKey{}, err
	}

	return sshKey{
		Type:    out.Type(),
		Key:     out.Marshal(),
		Comment: comments,
	}, nil
}

func parseKeys(contents string) sshKeys {
	var ret []sshKey

	split := strings.Split(contents, "\n")
	for _, row := range split {
		key, err := parseKey([]byte(row))
		if err != nil {
			continue
		}

		ret = append(ret, key)
	}

	return ret
}

func promptYN(label string, defaultSelection bool) bool {
	yesOption := "y"
	noOption := "n"

	if defaultSelection {
		yesOption = "Y"
	} else {
		noOption = "N"
	}

	for {
		fmt.Printf("%s [%s/%s]: ", label, yesOption, noOption)

		reader := bufio.NewReader(os.Stdin)

		text, err := reader.ReadString('\n')
		if err != nil {
			panic(fmt.Sprintf("error while reading console input: %v", err))
		}

		text = strings.TrimSpace(text)

		if text == "" {
			return defaultSelection
		}

		if strings.EqualFold(yesOption, text) {
			return true
		}

		if strings.EqualFold(noOption, text) {
			return false
		}
	}
}

type authorizedKeysFile struct {
	file  string
	perms fs.FileMode
}

func newAuthorizedKeysFile(file string) authorizedKeysFile {
	return authorizedKeysFile{
		file:  file,
		perms: 0600,
	}
}

func (aks authorizedKeysFile) GetAuthorizedKeys() (sshKeys, error) {
	var currentKeys sshKeys

	contents, err := os.ReadFile(aks.file)
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		return nil, err
	} else if err == nil {
		currentKeys = parseKeys(string(contents))
	}

	return currentKeys, nil
}

func (aks authorizedKeysFile) SetAuthorizedKeys(k sshKeys) error {
	return renameio.WriteFile(aks.file, k.marshal(), aks.perms)
}

func getGitlabKeys(users []string) (sshKeys, error) {
	gc := gitlab.NewClient()

	var keysLock sync.Mutex
	var keys sshKeys

	errs, ctx := errgroup.WithContext(context.Background())

	for _, user := range users {
		user := user

		errs.Go(func() error {
			gitlabKeys, err := gc.GetPublicKeys(ctx, user)
			if err != nil {
				return fmt.Errorf("error retrieving keys from GitLab for user '%s': %w", user, err)
			}

			k, err := convertGitlabKeys(gitlabKeys)
			if err != nil {
				return fmt.Errorf("error while parsing keys from GitLab for user '%s': %w", user, err)
			}

			keysLock.Lock()
			keys = append(keys, k...)
			keysLock.Unlock()

			return nil
		})
	}

	if err := errs.Wait(); err != nil {
		return nil, err
	}

	return keys, nil
}

func convertGitlabKey(pk gitlab.PublicKey) (sshKey, error) {
	parsed, err := parseKey([]byte(pk.Key))
	if err != nil {
		return sshKey{}, fmt.Errorf("couldn't parse key '%s': %w", pk.Title, err)
	}

	parsed.Comment = pk.Title

	return parsed, nil
}

func convertGitlabKeys(pks []gitlab.PublicKey) (sshKeys, error) {
	var keys sshKeys

	for _, gitlabKey := range pks {
		k, err := convertGitlabKey(gitlabKey)
		if err != nil {
			return nil, err
		}

		keys = append(keys, k)
	}

	return keys, nil
}

func selectKeysToAdd(keys sshKeys) (selected, skipped sshKeys) {
	for _, key := range keys {
		prompt := color.GreenString(fmt.Sprintf("Add key %v?", key))

		shouldAddKey := promptYN(prompt, true)
		if shouldAddKey {
			selected = append(selected, key)
		} else {
			skipped = append(skipped, key)
		}
	}

	return selected, skipped
}

func selectKeysToDelete(keys sshKeys) (selected, skipped sshKeys) {
	for _, key := range keys {
		prompt := color.RedString(fmt.Sprintf("Remove key %v?", key))

		shouldDeleteKey := promptYN(prompt, true)
		if shouldDeleteKey {
			selected = append(selected, key)
		} else {
			skipped = append(skipped, key)
		}
	}

	return selected, skipped
}

func fingerprintInSlice(s Fingerprint, arr []Fingerprint) bool {
	for _, item := range arr {
		if item == s {
			return true
		}
	}

	return false
}

type configFile struct {
	file  string
	perms fs.FileMode
}

func newConfigFile(file string) configFile {
	return configFile{
		file:  file,
		perms: 0600,
	}
}

func (cf configFile) SetConfig(cfg Config) error {
	data, err := yaml.Marshal(&cfg)
	if err != nil {
		return err
	}

	return renameio.WriteFile(cf.file, data, cf.perms)
}

type History []HistoryEntry

func (h History) AddedFingerprints() []Fingerprint {
	var f []Fingerprint

	for _, entry := range h {
		if entry.Action == HistoryEntryActionAdded {
			f = append(f, entry.Fingerprint)
		}
	}

	return f
}

type HistoryEntry struct {
	Action      HistoryEntryAction `yaml:"action,omitempty"`
	Fingerprint Fingerprint        `yaml:"fingerprint,omitempty"`
	Time        time.Time          `yaml:"time,omitempty"`
}

type HistoryEntryAction string

const (
	HistoryEntryActionAdded   HistoryEntryAction = "added"
	HistoryEntryActionRemoved HistoryEntryAction = "removed"
)

func newHistoryEntry(f Fingerprint, a HistoryEntryAction) HistoryEntry {
	return HistoryEntry{
		Action:      a,
		Fingerprint: f,
		Time:        time.Now().Round(time.Second),
	}
}

func newHistoryEntries(ff []Fingerprint, a HistoryEntryAction) []HistoryEntry {
	var he []HistoryEntry

	for _, f := range ff {
		he = append(he, newHistoryEntry(f, a))
	}

	return he
}

type historyFile struct {
	file  string
	perms fs.FileMode
}

func newHistoryFile(file string) historyFile {
	return historyFile{
		file:  file,
		perms: 0600,
	}
}

func (hf historyFile) GetHistory() (History, error) {
	contents, err := os.ReadFile(hf.file)
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		return History{}, err
	}

	var h History
	if err := yaml.Unmarshal(contents, &h); err != nil {
		return History{}, err
	}

	return h, nil
}

func (hf historyFile) SetHistory(h History) error {
	contents, err := yaml.Marshal(h)
	if err != nil {
		return err
	}

	return renameio.WriteFile(hf.file, contents, hf.perms)
}
