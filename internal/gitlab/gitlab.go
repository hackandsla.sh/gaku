package gitlab

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

type Client struct {
	http.Client
	baseURL string
}

func NewClient() Client {
	return Client{
		Client: http.Client{
			Timeout: 60 * time.Second,
		},
		baseURL: "https://gitlab.com/api/v4",
	}
}

func (c Client) GetPublicKeys(ctx context.Context, user string) ([]PublicKey, error) {
	req, err := http.NewRequestWithContext(
		ctx,
		http.MethodGet,
		fmt.Sprintf("%s/users/%s/keys", c.baseURL, user),
		nil,
	)
	if err != nil {
		return nil, err
	}

	res, err := c.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error getting gitlab info: %w", err)
	}

	if res.StatusCode != http.StatusOK {
		return nil, newErrAPI(res.StatusCode)
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	var ret []PublicKey
	if err := json.Unmarshal(body, &ret); err != nil {
		return nil, err
	}

	return ret, nil
}

type PublicKey struct {
	ID        int       `json:"id"`
	Title     string    `json:"title"`
	CreatedAt time.Time `json:"created_at"`
	ExpiresAt time.Time `json:"expires_at"`
	Key       string    `json:"key"`
}

type ErrAPI struct {
	code int
}

func newErrAPI(code int) ErrAPI {
	return ErrAPI{code: code}
}

func (e ErrAPI) Error() string {
	return fmt.Sprintf("GitLab returned error code '%d'", e.code)
}
