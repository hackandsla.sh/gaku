# gaku

GitLab Authorized-Key Updater

## Overview

This tool manages SSH key deployments on your servers through GitLab. It does this by looking through your public keys stored on GitLab, then adding any missing keys to your `authorized_keys` file. It will:

- Prompt to add new keys that haven't been added yet
- Automatically remove keys that were removed or expired

## Quickstart

```bash
# If you trust me ;)
curl https://gitlab.com/hackandsla.sh/gaku/-/raw/main/get-gaku.sh | sudo bash

gaku -u <my_gitlab_username>
```

This will prompt you to add your public keys from GitLab and save your configuration to `~/.config/gaku/gaku.yaml`

To update your authorized keys:

```bash
gaku
```

## Installation

There are several ways you can install this tool:

1. Via the installation script (root):

    ```bash
    curl https://gitlab.com/hackandsla.sh/gaku/-/raw/main/get-gaku.sh | sudo bash
    ```

    This will install Gaku to `/usr/local/bin/gaku`

2. Via the installation script (non-root):

    ```bash
    curl https://gitlab.com/hackandsla.sh/gaku/-/raw/main/get-gaku.sh | DESTINATION=~/bin bash
    ```

    This will install Gaku to whatever destination folder you have defined as `DESTINATION`

3. Manual download from the [releases page](https://gitlab.com/hackandsla.sh/gaku/-/releases)

4. Manual building with the Go CLI:

    ```bash
    go install gitlab.com/hackandsla.sh/gaku@latest
    ```

## Auto-Deletion

Gaku has an auto-deletion capability that allows you to non-interactively remove keys that have been removed from GitLab. This is to allow automatically removing keys that have been revoked or deleted. You can use it like so:

```bash
gaku --auto-delete
```

The best way to use this is via a scheduled task manager, like cron:

```cron
# In your user crontab, which you can edit using "crontab -e"
*/5 * * * * /usr/local/bin/gaku --auto-delete 2>&1 >> ~/.config/gaku/gaku-log
```

This job will poll GitLab every 5 minutes to determine if there are any keys it should delete from the `authorized_keys`.

NOTE, the auto-deletion will only remove keys that it previously added. It will NOT remove any keys previously existing on the system, so you don't need to worry about any previously-existing keys getting deleted by this.

## Why use this tool?

Most of the time, the SSH keys I use for GitLab are the same ones I use to access my remote servers. Pulling them from GitLab is a lot easier than manually pushing them one-at-a-time.

GitLab has some support for this: if you go to `gitlab.com/<myusername>.keys`, it will return all your public keys in the format required for `authorized_keys`. However, it's painful for a couple of reasons:

1. All the comments in the generated file only list the GitLab user's name, not the key title. This loses the context of where the key comes from.
2. This isn't great for multiple GitLab users. In my case, I have both a work account and a personal account that I want to grab keys for, so I need something that will grab each user's keys.
3. It doesn't show the diff of what's changing. This is a potential security concern, since I want to know explicitly whenever something is added to my `authorized_keys`.
4. If a key has been removed or revoked, it takes manual intervention to sync the removal to my servers. I want a system which allows me to automatically remove keys from my infrastructure if the key were to become compromised.

Gaku addresses each of these concerns:

1. It shows the key title/comment when prompting to add the key, so you know which key it is.
2. It supports multiple GitLab users.
3. It prompts for each key being added or removed, making it explicitly clear when something is changes.
4. It supports autodeletion, which enables keys that have been removed on GitLab to immediately become removed on servers.

## License

This software is MIT licensed. See [LICENSE](LICENSE) for more details.

Copyright (c) 2021 Trevor Taubitz
